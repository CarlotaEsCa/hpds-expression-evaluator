package evaluator;

public interface Expression<ReturningType> {
        public ReturningType evaluate();
}