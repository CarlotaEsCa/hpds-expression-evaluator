package evaluator;

public abstract class BinaryOperation<ReturningType> implements Expression<ReturningType> {

    protected Expression<ReturningType> left;
    protected Expression<ReturningType> right;

    public BinaryOperation(Expression<ReturningType> left, Expression<ReturningType> right) {
        this.left = left;
        this.right = right;
    }
    
}
