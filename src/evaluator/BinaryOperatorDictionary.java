package evaluator;

public class BinaryOperatorDictionary {

    public BinaryOperatorDictionary() {
    }

    public BinaryOperator getOperator(String operation, Object leftOperand, Object rightOperand) {
        try {
            Class operatorClass = getClass(operation, leftOperand, rightOperand);
            return (BinaryOperator) operatorClass.newInstance();
        } catch (Exception e) {
            return null;
        }

    }

    private String getSignature(String operation, Object left, Object right) {
        return "evaluator.operators." + operation.toLowerCase() + "." + 
                left.getClass().getSimpleName() + right.getClass().getSimpleName() + operation;
    }

    private Class getClass(String operation, Object left, Object right) {
        try {
            return Class.forName(getSignature(operation, left, right));
        } catch (ClassNotFoundException ex) {
            return null;
        }
    }
}
