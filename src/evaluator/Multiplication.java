package evaluator;

public class Multiplication extends BinaryOperation<Object> {

    public Multiplication(Expression left, Expression right) {
        super(left, right);
    }

    @Override
    public Object evaluate() {
        return evaluate(left.evaluate(), right.evaluate());
    }
    
    public Object evaluate(Object left, Object right) {
        return getOperator(left, right).calculate(left, right);
    }
    
    private BinaryOperator getOperator(Object left, Object right) {
        BinaryOperatorDictionary dictionary = new BinaryOperatorDictionary();
        return dictionary.getOperator("Multiplication", left, right);
    }
    
}
