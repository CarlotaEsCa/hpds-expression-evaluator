package evaluator.operators.addition;

import evaluator.BinaryOperator;

public class IntegerIntegerAddition implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer)left+(Integer)right;
    }
    
}
