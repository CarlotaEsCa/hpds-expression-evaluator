package evaluator.operators.addition;

import evaluator.BinaryOperator;

public class IntegerDoubleAddition implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer)left+(Double)right;
    }
}
