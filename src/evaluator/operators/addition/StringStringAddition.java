package evaluator.operators.addition;

import evaluator.BinaryOperator;

public class StringStringAddition implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (String)left+(String)right;
    }
    
}