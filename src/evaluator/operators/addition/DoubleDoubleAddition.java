package evaluator.operators.addition;

import evaluator.BinaryOperator;

public class DoubleDoubleAddition implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Double)left+(Double)right;
    }
    
}
