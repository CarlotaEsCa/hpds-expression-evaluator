package evaluator.operators.addition;

import evaluator.BinaryOperator;

public class DoubleIntegerAddition implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Double)left+(Integer)right;
    }
}
