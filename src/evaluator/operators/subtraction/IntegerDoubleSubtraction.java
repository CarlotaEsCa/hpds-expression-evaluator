package evaluator.operators.subtraction;

import evaluator.BinaryOperator;

public class IntegerDoubleSubtraction implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer)left-(Double)right;
    }
    
}
