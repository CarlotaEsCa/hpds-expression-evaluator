package evaluator.operators.subtraction;

import evaluator.BinaryOperator;

public class DoubleDoubleSubtraction implements BinaryOperator{
    
    @Override
    public Object calculate(Object left, Object right) {
        return (Double)left-(Double)right;
    }
}
