package evaluator.operators.subtraction;

import evaluator.BinaryOperator;

public class IntegerIntegerSubtraction implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer)left-(Integer)right;
    }
    
}
