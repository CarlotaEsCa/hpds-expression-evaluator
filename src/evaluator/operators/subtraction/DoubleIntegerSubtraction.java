package evaluator.operators.subtraction;

import evaluator.BinaryOperator;

public class DoubleIntegerSubtraction implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Double)left-(Integer)right;
    }
    
}
