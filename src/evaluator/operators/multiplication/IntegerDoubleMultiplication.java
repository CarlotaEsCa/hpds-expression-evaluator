package evaluator.operators.multiplication;

import evaluator.BinaryOperator;

public class IntegerDoubleMultiplication implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer)left*(Double)right;
    }
    
}
