package evaluator.operators.multiplication;

import evaluator.BinaryOperator;

public class DoubleDoubleMultiplication implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Double)left*(Double)right;
    }
    
}
