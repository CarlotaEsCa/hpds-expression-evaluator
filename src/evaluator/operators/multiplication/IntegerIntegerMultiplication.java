package evaluator.operators.multiplication;

import evaluator.BinaryOperator;

public class IntegerIntegerMultiplication implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer)left*(Integer)right;
    }
    
}
