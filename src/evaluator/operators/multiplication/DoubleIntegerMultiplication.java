package evaluator.operators.multiplication;

import evaluator.BinaryOperator;

public class DoubleIntegerMultiplication implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Double)left*(Integer)right;
    }
    
}
