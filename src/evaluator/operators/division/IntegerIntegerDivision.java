package evaluator.operators.division;

import evaluator.BinaryOperator;

public class IntegerIntegerDivision implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer)left/(Integer)right;
    }
    
}
