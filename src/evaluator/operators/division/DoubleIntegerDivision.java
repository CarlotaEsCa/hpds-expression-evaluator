package evaluator.operators.division;

import evaluator.BinaryOperator;

public class DoubleIntegerDivision implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Double)left/(Integer)right;
    }
}
