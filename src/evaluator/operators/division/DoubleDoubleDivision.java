package evaluator.operators.division;

import evaluator.BinaryOperator;

public class DoubleDoubleDivision implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Double)left/(Double)right;
    }
    
}
