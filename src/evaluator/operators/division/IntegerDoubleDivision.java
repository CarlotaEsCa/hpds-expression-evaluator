package evaluator.operators.division;

import evaluator.BinaryOperator;

public class IntegerDoubleDivision implements BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer)left/(Double)right;
    }
}
