package evaluator;

public class Constant<ReturningType> implements Expression<ReturningType> {
    private final ReturningType value;

    public Constant(ReturningType i) {
        this.value = i;
    }

    @Override
    public ReturningType evaluate() {
        return value;
    }
    
}
