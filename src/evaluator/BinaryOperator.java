package evaluator;

public interface BinaryOperator {

    public Object calculate(Object left, Object right);
}
