package evaluator;

public class Division extends BinaryOperation<Object> {

    public Division(Expression left, Expression right) {
        super(left, right);
    }

    @Override
    public Object evaluate() {
        return evaluate(left.evaluate(), right.evaluate());
    }
    
    public Object evaluate(Object left, Object right) {
        return getOperator(left, right).calculate(left, right);
    }
    
    private BinaryOperator getOperator(Object left, Object right) {
        BinaryOperatorDictionary dictionary = new BinaryOperatorDictionary();
        return dictionary.getOperator("Division", left, right);
    }
    
}
