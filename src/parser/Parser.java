package parser;

import evaluator.Expression;

public class Parser {
    private final ParserStrategy parserStrategy;

    public Parser(ParserStrategy parserStrategy) {
        this.parserStrategy = parserStrategy;
    }
    
    public Expression parse(Token[] token){
        return parserStrategy.parse(token);
    }
    
}
