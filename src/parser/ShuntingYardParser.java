package parser;

import evaluator.Addition;
import evaluator.Constant;
import evaluator.Division;
import evaluator.Expression;
import evaluator.Multiplication;
import evaluator.Subtraction;
import parser.Token.Symbol;

public class ShuntingYardParser extends ParserStrategy {

    public ShuntingYardParser(ExpressionFactory factory) {
        super(factory);
    }

    @Override
    public Expression parse(Token[] tokens) {
        analyze(tokens);
        process();
        return expressions.pop();
    }

    private void analyze(Token[] tokens) {
        for (Token token : tokens)
            analyze(token);
    }

    private void analyze(Token token) {
        if (token instanceof Token.Constant)
            analyze((Token.Constant) token);
        if (token instanceof Token.Symbol)
            analyze((Token.Symbol) token);

    }

    private Expression analyze(Token.Constant token) {
        return expressions.push(buildConstant(token));
    }

    private void analyze(Token.Symbol token) {
        if (newSymbolHasLessOrSamePrecedenceThan((Token.Symbol) token) && token != Token.LEFTPARENTHESIS)
            process(symbols.pop());
        else if (token == Token.RIGHTPARENTHESIS){
            processParenthesis();
            return;
        }
        symbols.push((Token.Symbol) token);
    }

    private void process() {
        while (!symbols.isEmpty())
            process(symbols.pop());
    }

    private void process(Token.Symbol symbol) {
        Expression right = expressions.pop();
        Expression left = expressions.pop();
        createBinaryOperation(symbol, left, right);
    }

    private Constant buildConstant(Token.Constant token) {
        return new Constant(token.getValue());
    }

    private boolean newSymbolHasLessOrSamePrecedenceThan(Token.Symbol token) {
        if (symbols.isEmpty()) return false;
        return token.hasLessOrSamePrecedenceThan(symbols.peek());
    }

    private void createBinaryOperation(Symbol symbol, Expression left, Expression right) {
        ExpressionFactory expressionFactory = new ExpressionFactory();
        expressions.push(expressionFactory.buildExpression(symbol, left, right));
    }

    private void processParenthesis() {
        while(symbols.peek() != Token.LEFTPARENTHESIS)
            process(symbols.pop());
        symbols.pop();
    }
}
