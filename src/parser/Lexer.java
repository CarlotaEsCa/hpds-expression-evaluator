package parser;

import java.util.ArrayList;
import java.util.List;
import static parser.Token.*;

public class Lexer {
    private String expression;
    private List<Token> tokens;
    
    public Lexer(String expression) {
        this.expression = expression;
        this.tokens = new ArrayList<Token>();
    }
    
    public Token[] turnStringIntoTokens() throws ExpressionValidator.IncorrectExpression{
        ExpressionValidator expressionValidator = new ExpressionValidator(expression);
        expressionValidator.check();
        for (int position = 0; position < expression.length(); position++) {
            if (isASymbol(position)){
                processSubstringsIntoTokens(position);
                position = -1;
            }
        }
        if (expression.length() > 0)
            tokens.add(transformIntoConstantToken(expression));
        return tokens.toArray(new Token[tokens.size()]);
    }

    private boolean isASymbol(int position) {
        return Token.getSymbols().containsKey(String.valueOf(expression.charAt(position)));
    }
    
    private void processSubstringsIntoTokens(int position) {
        if (position > 0)
            processConstantSubstring(position);
        processOperatorSubstring(position);
        cutProcessedSubstrings(position);
    }
    
    private void processConstantSubstring(int position) {
        tokens.add(transformIntoConstantToken(expression.substring(0,position)));
    }

    private void processOperatorSubstring(int position) {
        tokens.add(transformIntoSymbolToken(expression.substring(position,position+1)));
    }
    
    private Token.Constant transformIntoConstantToken(String constant){
        if(constant.contains("."))
            return constant(Double.parseDouble(constant));
        if(constant.contains("\""))
            return constant(constant.replace("\"", ""));
        else
            return constant(Integer.parseInt(constant));
    }
        
    private Token.Symbol transformIntoSymbolToken(String token){
        return symbol(token);
    }

    private void cutProcessedSubstrings(int position) {
        expression=expression.substring((position+1));
    }

}
