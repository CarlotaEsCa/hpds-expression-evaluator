package parser;

public class ExpressionValidator {
    private String expression;
    private int parenthesisChecker;

    public ExpressionValidator(String expression) {
        this.expression = expression;
        parenthesisChecker=0;
    }
    
     public void check() throws IncorrectExpression{
         boolean lastCharacterIsSymbol = true;
         for (int position = 0; position < expression.length(); position++) {
             if(isASymbol(position)){
                 if(isParenthesis(getCharacter(position)))
                     checkParenthesis(getCharacter(position));
                 else if(lastCharacterIsSymbol)
                     throw new IncorrectExpression("Expression is not correct");
                 else
                    lastCharacterIsSymbol=true;
             }else
                 lastCharacterIsSymbol = false;
         }
         checkParenthesis();
     }
     
     private boolean isASymbol(int position){
         return Token.getSymbols().containsKey(String.valueOf(getCharacter(position)));
     }

    private boolean isParenthesis(Character character) {
        return character == '(' || character == ')';
    }
    
    private Character getCharacter(int position){
        return expression.charAt(position);
    }
    
    private void checkParenthesis(Character parenthesis) throws IncorrectExpression{
        if(parenthesis == '(')
           parenthesisChecker++;
        else
            parenthesisChecker--;
        if (parenthesisChecker < 0){
            throw new IncorrectExpression("Expression is not correct");
        }
    }

    private void checkParenthesis() throws IncorrectExpression {
        if(parenthesisChecker != 0)
            throw new IncorrectExpression("Expression is not correct");
    }
     
     public class IncorrectExpression extends Exception{
         public IncorrectExpression(String message){
             super(message);
         }
     }
     
     
}
