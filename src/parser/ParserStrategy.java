package parser;

import evaluator.Expression;
import java.util.Stack;

public abstract class ParserStrategy {
    protected final Stack<Expression> expressions;
    protected final ExpressionFactory factory;
    protected final Stack<Token.Symbol> symbols;

    public ParserStrategy(ExpressionFactory factory) {
        this.symbols = new Stack<>();
        this.expressions = new Stack<>();
        this.factory = factory;
    }
    
    public abstract Expression parse(Token[] tokens);
    
}
