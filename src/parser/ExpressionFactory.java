package parser;

import evaluator.Addition;
import evaluator.Division;
import evaluator.Expression;
import evaluator.Multiplication;
import evaluator.Subtraction;
import java.util.HashMap;
import parser.Token.Symbol;

public class ExpressionFactory {
    private HashMap<Symbol, Builder> builders;
    
    public ExpressionFactory(){
        builders = new HashMap<>();
        builders.put(Token.ADD, new Builder(){
            @Override
            public Expression build(Expression left, Expression right) {
                return new Addition(left,right);
            }
        });
        builders.put(Token.MINUS, new Builder(){
            @Override
            public Expression build(Expression left, Expression right) {
                return new Subtraction(left,right);
            }
        });

        builders.put(Token.MULTIPLY, new Builder(){
            @Override
            public Expression build(Expression left, Expression right) {
                return new Multiplication(left,right);
            }
        });
        
        builders.put(Token.DIVIDE, new Builder(){
            @Override
            public Expression build(Expression left, Expression right) {
                return new Division(left,right);
            }
        });
    }
    
    public Expression buildExpression(Symbol symbol, Expression left, Expression right){
        return builders.get(symbol).build(left,right);     
    }
    
    
    private interface Builder{
        public Expression build(Expression left, Expression right);
    }
    
}
