package parser;

import java.util.HashMap;
import java.util.Map;

public abstract class Token {
    private static Map<String, Symbol> symbols;

    public static Map<String, Symbol> getSymbols() {
        return symbols;
    }
    
    public static <Type>Constant constant(Type value){
        return new Constant(value);
    }
    
    public static Symbol symbol(String chars){
        return symbols.get(chars);
    }
    
    
    public static class Constant<Type> extends Token{
        private final Type value;

        private Constant(Type value) {
            this.value = value;
        }

        public Type getValue() {
            return value;
        }

        public boolean equals(Token.Constant constant) {
            return value.equals(constant.value);
        }
    }
    
    public static final Symbol ADD =  new Symbol(0);
    public static final Symbol MINUS = new Symbol(0);
    public static final Symbol MULTIPLY =  new Symbol(1);
    public static final Symbol DIVIDE = new Symbol(1);
    public static final Symbol LEFTPARENTHESIS = new Symbol(-1);
    public static final Symbol RIGHTPARENTHESIS = new Symbol(5);
    
    
    static {
        symbols = new HashMap<>();
        symbols.put("+", ADD);
        symbols.put("*", MULTIPLY);
        symbols.put("/", DIVIDE);
        symbols.put("-", MINUS);
        symbols.put("(", LEFTPARENTHESIS);
        symbols.put(")", RIGHTPARENTHESIS);
   }
    
    public static class Symbol extends Token {
        private final int precedence;

        private Symbol(int precedence) {
            this.precedence = precedence;
        }
        
        public boolean hasLessOrSamePrecedenceThan(Symbol symbol) {
            return symbol.precedence >= this.precedence;
        }
        
        public boolean equals(Token.Symbol symbol){
            return this == symbol;
        }


    }
}