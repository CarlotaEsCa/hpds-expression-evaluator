package parser;

import evaluator.Addition;
import evaluator.Expression;
import evaluator.ExpressionEvaluatorTest;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import parser.ExpressionValidator.IncorrectExpression;
import static parser.Token.*;

public class ParserTest {

    public ParserTest() {
    }

    @Test
    public void singleExpressionParser() {
        ExpressionFactory factory = mock(ExpressionFactory.class);
        ShuntingYardParser parser = new ShuntingYardParser(factory);
        Token[] tokens = {
            constant(1),
            symbol("+"),
            constant(2)
        };
        Expression expression = parser.parse(tokens);
        assertEquals(new Addition(new evaluator.Constant(1), new evaluator.Constant(2)).evaluate(), expression.evaluate());
    }
    
    @Test
    public void twoOperandsExpressionParser(){
        ExpressionFactory factory = mock(ExpressionFactory.class);
        ShuntingYardParser parser = new ShuntingYardParser(factory);
        Token[] tokens = {
            constant(1),
            symbol("+"),
            constant(2),
            symbol("-"),
            constant(1)
        };
        Expression expression = parser.parse(tokens);
        assertEquals(2, expression.evaluate());
    }
    
    @Test
    public void threeOperandsExpressionParser(){
        ExpressionFactory factory = mock(ExpressionFactory.class);
        ShuntingYardParser parser = new ShuntingYardParser(factory);
        Token[] tokens = {
            constant(1),
            symbol("+"),
            constant(2),
            symbol("-"),
            constant(3),
            symbol("/"),
            constant(1)
        };
        Expression expression = parser.parse(tokens);
        assertEquals(0, expression.evaluate());
    }
    
    @Test
    public void fourOperandsWithPrecedenceParser(){
        ExpressionFactory factory = mock(ExpressionFactory.class);
        ShuntingYardParser parser = new ShuntingYardParser(factory);
        Token[] tokens = {
            constant(1),
            symbol("*"),
            constant(2),
            symbol("+"),
            constant(3),
            symbol("*"),
            constant(1),
            symbol("-"),
            constant(2)
        };
        Expression expression = parser.parse(tokens);
        assertEquals(3, expression.evaluate());
    }
    
    @Test
    public void parenthesisTest(){
        ExpressionFactory factory = mock(ExpressionFactory.class);
        ShuntingYardParser parser = new ShuntingYardParser(factory);
        Token[] tokens = {
            symbol("("),
            constant(4),
            symbol("+"),
            constant(9),
            symbol(")")
        };
        Expression expression = parser.parse(tokens);
        assertEquals(13, expression.evaluate());
    }
    
    @Test
    public void complexParenthesisTest(){
        ExpressionFactory factory = mock(ExpressionFactory.class);
        ShuntingYardParser parser = new ShuntingYardParser(factory);
        Token[] tokens = {
            constant(2),
            symbol("*"),
            symbol("("),
            constant(4),
            symbol("+"),
            constant(9),
            symbol(")")
        };
        Expression expression = parser.parse(tokens);
        assertEquals(26, expression.evaluate());
    }
    
            public void stringTest() {
        ExpressionFactory factory = mock(ExpressionFactory.class);
        ShuntingYardParser parser = new ShuntingYardParser(factory);
        Token[] tokens = {constant("Hello"), symbol("+"), constant(" goodbye")};
        Expression expression = parser.parse(tokens);
        assertEquals("Hello goodbye", expression.evaluate());
    }


    
}
