package parser;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ExpressionValidatorTest {
    
    public ExpressionValidatorTest() {
    }

    public void parenthesisIncorrectExpression() {
        try {
            Lexer lexicon = new Lexer("(4+3)");
            Token[] tokens = lexicon.turnStringIntoTokens();
            fail();
        } catch (ExpressionValidator.IncorrectExpression incorrectExpression) {
            assertEquals("Expression is not correct", incorrectExpression.getMessage());
        }
    }

    public void incorrectExpression(){
        try{
            Lexer lexicon = new Lexer("4++5");
            Token[] tokens = lexicon.turnStringIntoTokens();
            fail();
        } catch (ExpressionValidator.IncorrectExpression incorrectExpression) {
            assertEquals("Expression is not correct", incorrectExpression.getMessage());
        }
    }

    public void anotherIncorrectExpression() {
        try {
            Lexer lexicon = new Lexer(")4+5(");
            Token[] tokens = lexicon.turnStringIntoTokens();
            fail();
        } catch (ExpressionValidator.IncorrectExpression incorrectExpression) {
            assertEquals("Expression is not correct", incorrectExpression.getMessage());
        }
    }
    
    
}