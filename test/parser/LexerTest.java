package parser;

import evaluator.Expression;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import parser.ExpressionValidator.IncorrectExpression;
import static parser.Token.constant;
import static parser.Token.symbol;
import static parser.Token.Constant;

public class LexerTest {
    
    public LexerTest() {
    }

    @Test
    public void simpleLexerTest() throws IncorrectExpression {
        Token[] lexer = new Lexer("1.0+2+3.0").turnStringIntoTokens();
        Token[] tokens = {
            constant(1.0),
            symbol("+"),
            constant(2),
            symbol("+"),
            constant(3.0)
        };
        for (int i = 0; i < tokens.length; i++) {
            if(lexer[i].getClass()==Token.Constant.class)
                assertEquals(true, ((Token.Constant)tokens[i]).equals((Token.Constant)lexer[i]));
            else
                assertEquals(true, ((Token.Symbol)tokens[i]).equals((Token.Symbol)lexer[i]));
        }
        
    }
    
    @Test
    public void anotherSimpleLexerTest() throws IncorrectExpression {
        Token[] lexer = new Lexer("30-2-8").turnStringIntoTokens();
        Token[] tokens = {
            constant(30),
            symbol("-"),
            constant(2),
            symbol("-"),
            constant(8)
        };
        for (int i = 0; i < tokens.length; i++) {
            if(lexer[i].getClass()==Token.Constant.class)
                assertEquals(true, ((Token.Constant)tokens[i]).equals((Token.Constant)lexer[i]));
            else
                assertEquals(true, ((Token.Symbol)tokens[i]).equals((Token.Symbol)lexer[i]));
        }
    }
    

    
    @Test
    public void finalComplexTest() throws IncorrectExpression {
        Token[] lexer = new Lexer("(4+5)*4+8").turnStringIntoTokens();
        Token[] tokens = {
            symbol("("),
            constant(4),
            symbol("+"),
            constant(5),
            symbol(")"),
            symbol("*"),
            constant(4),
            symbol("+"),
            constant(8)
        };
        for (int i = 0; i < tokens.length; i++) {
            if(lexer[i].getClass()==Token.Constant.class)
                assertEquals(true, ((Token.Constant)tokens[i]).equals((Token.Constant)lexer[i]));
            else
                assertEquals(true, ((Token.Symbol)tokens[i]).equals((Token.Symbol)lexer[i]));
        }
    }

    @Test
    public void lexiconStringTest() throws IncorrectExpression {
        Token[] lexer = new Lexer("\"hello\"+\" goodbye\"").turnStringIntoTokens();
        Token[] tokens = {
            constant("hello"),
            symbol("+"),
            constant(" goodbye")
        };
        for (int i = 0; i < tokens.length; i++) {
            if(lexer[i].getClass()==Token.Constant.class)
                assertEquals(true, ((Token.Constant)tokens[i]).equals((Token.Constant)lexer[i]));
            else
                assertEquals(true, ((Token.Symbol)tokens[i]).equals((Token.Symbol)lexer[i]));
        }
    }
}