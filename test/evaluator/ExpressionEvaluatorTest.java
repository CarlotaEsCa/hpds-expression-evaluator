package evaluator;

import org.junit.Test;
import static org.junit.Assert.*;

public class ExpressionEvaluatorTest {
    
    public ExpressionEvaluatorTest() {
    }
    
        @Test
        public void constantsTest(){
            assertEquals(2,new Constant(2).evaluate());
            assertEquals(30,new Constant(30).evaluate());
            assertEquals(5.0,new Constant(5.0).evaluate());
        }
        
        @Test
        public void additionTest(){
            assertEquals(5, new Addition(new Constant(3), new Constant(2)).evaluate());
            assertEquals(6.0, new Addition(new Constant(3.0), new Constant(3.0)).evaluate());
            assertEquals(4.0, new Addition(new Constant(3.0), new Constant(1)).evaluate());
            assertEquals(3.0, new Addition(new Constant(1), new Constant(2.0)).evaluate());
        }
        
        @Test
        public void multiplicationTest(){
            assertEquals(6, new Multiplication(new Constant(3), new Constant(2)).evaluate());
            assertEquals(9.0, new Multiplication(new Constant(3.0), new Constant(3.0)).evaluate());
            assertEquals(3.0, new Multiplication(new Constant(3.0), new Constant(1)).evaluate());
            assertEquals(2.0, new Multiplication(new Constant(1), new Constant(2.0)).evaluate());
        }
        
        @Test
        public void subtractionTest(){
            assertEquals(3, new Subtraction(new Constant(5), new Constant(2)).evaluate());
            assertEquals(0.0, new Subtraction(new Constant(3.0), new Constant(3.0)).evaluate());
            assertEquals(-2.0, new Subtraction(new Constant(3.0), new Constant(5)).evaluate());
            assertEquals(-1.0, new Subtraction(new Constant(1), new Constant(2.0)).evaluate());
        }
        
        @Test
        public void divisionTest(){
            assertEquals(2, new Division(new Constant(5), new Constant(2)).evaluate());
            assertEquals(1.0, new Division(new Constant(3.0), new Constant(3.0)).evaluate());
            assertEquals(3.0, new Division(new Constant(6.0), new Constant(2)).evaluate());
            assertEquals(0.5, new Division(new Constant(1), new Constant(2.0)).evaluate());
        }
        
        @Test
        public void twoOperandsTest(){
            assertEquals(6.0, new Addition(new Constant(5), new Subtraction(new Constant(2), new Constant(1.0))).evaluate());
            assertEquals(15.0, new Multiplication(new Division(new Constant(25.0), new Constant(5)), new Constant(3.0)).evaluate());
        }
}
